# How to build

## Prerequisites

### Package managers

Pods: sudo gem install cocoapods

### Setup instructions
Clone repo
In terminal: pod update on repo folder
Open workspace

## Notes:

### Architecture: 
- Design pattern: MVVM, biding with Box for view properties and delegates for events
- SOLID
- Some personal ordering and structures
- Network layer done with Moya
- Models with Structs and classes mixed
- Given, When, Then testing pattern


### Models:
Used classes to improve performance, avoiding searching for the items to modify it’s amount.
Used Contiguousarray to improve performance, since it hold class references
Rest its just Structs (in sack, memory leak, thread safe, performance, etc)
Also struct are used as references to original one in the stack, since are not written/modify. To save space and performance.

-StoreItem: a Class with normal  item with all its info
-CartItem: a Class with item data needed for check out (the amount of items in the cart and a reference to the StoreItems). Used as data for store TableView.

-Promotion: a Struct with all the promotion info (made up since was not given, more details in model)
-CartPromotion: a Struct with data needed for checkout (amount of items to apply this promo, price after discount and promotion name)

-PromotionsByItems: a Set of items code, that hold an array of all promotions of that item. Easy to search all promos for a specific item

-CartItemWithBestPromotions: Array of tuples of CartItems and CartPromotions that must be apply to the current item. Used as data for cart TableView.


### Table handling:
Separate table logic from view controller, in tableHandler. More clean and easy to read views (view controllers)

Box binding:
Copy from Raywenderlich 2016 conference, very handy and easy to understand for all senioritis level. No overhead like with react swift.

### Utils:
Core part of the app. Focuses on performance and readable code. Calculate total price, PromotionsByItems and CartItemWithBestPromotions


### Testing:
Tested viewModels, Utils, etc. All with sample data and Mocs.
Performance test of calculating all the discounts and final price, at check out. With 10.000+ items, 3.333.333 of each and 10.000+ promotions, with 10.000 applicable items. Average result on iPhone X is 0.010 secs

### UI Testing:
Very simple happy path, should be configure to alway use sample data


## NOTE: all promotions/discounts from the server MUST be descendant since its discount %

Also simulate promotion from service with sample data, since its not given. And made up some structure logic. That can be easily change to service of promotions easy and fast. (Changing init of provider and writing  the end point).
