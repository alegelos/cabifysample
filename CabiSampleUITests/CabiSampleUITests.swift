//
//  CabiSampleUITests.swift
//  CabiSampleUITests
//
//  Created by Alejandro Gelos on 6/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import XCTest

class CabiSampleUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    func testHappyPath() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Voucher").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify T-Shirt").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify T-Shirt").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify T-Shirt").buttons["+"].tap()
        
        let button = tablesQuery.cells.containing(.staticText, identifier:"Cabify T-Shirt").buttons["+"]
        button.tap()
        button.tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Coffee Mug").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Coffee Mug").buttons["+"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Cabify Coffee Mug").buttons["+"].tap()
        app.buttons["Check Out"].tap()
        app.staticTexts["Total price: 136.375 €"].tap()
    }
}
