//
//  StoreServices.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/10/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
import Moya

enum StoreServies {
    case products
    case promotions
}

// MARK: - TargetType
extension StoreServies: TargetType {
    var baseURL: URL {
        guard let baseURL = URL(string: "https://gist.githubusercontent.com/") else{
            fatalError("Fail to get StoreServices base URL")
        }
        return baseURL
    }
    
    var path: String {
        switch self {
        case .products: return "palcalde/6c19259bd32dd6aafa327fa557859c2f/raw/ba51779474a150ee4367cda4f4ffacdcca479887/Products.json"
        case .promotions: return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .products, .promotions: return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .products:
            return data(fromFile: "ProductsSample", ext: "json")
        case .promotions:
            return data(fromFile: "Promotions", ext: "json")
        }
    }
    
    var task: Task {
        switch self {
        case .products, .promotions: return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .products, .promotions:
            return ["Content-Type": "application/json"]
        }
    }
    
}

// MARK: - Private
extension StoreServies {
    private func data(fromFile file: String, ext: String) -> Data {
        guard let url = Bundle.main.url(forResource: file, withExtension: ext),
            let data = try? Data(contentsOf: url) else {
                return Data()
        }
        return data
    }
}
