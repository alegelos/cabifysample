//
//  NetworkState.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

enum NetworkState<T: Codable, E: Error> {
    case loading, ready(T), error(E)
}
