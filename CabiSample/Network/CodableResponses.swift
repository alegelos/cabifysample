//
//  CodableResponses.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct StoreItemsResponse<T: Codable>: Codable {
    let products: [T]
}

struct StorePromotionsResponse<T: Codable>: Codable {
    let promotions: [T]
}
