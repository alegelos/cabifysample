//
//  ViewContrller+additions.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAlert(withTitle title: String = "", message: String = "", options: [String],
        completion: @escaping ((Int) -> Void) = {_ in } ){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        present(alertController, animated: true, completion: nil)
    }
}
