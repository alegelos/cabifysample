//
//  Sequence+additions.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/22/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

extension Sequence {
    func mapContiguousArray<T>(
        _ transform: (Element) throws -> T
    ) rethrows -> ContiguousArray<T> {
        let initialCapacity = underestimatedCount
        var result = ContiguousArray<T>()
        result.reserveCapacity(initialCapacity)
        
        var iterator = makeIterator()
        
        // Add elements up to the initial capacity without checking for regrowth.
        for _ in 0..<initialCapacity {
            result.append(try transform(iterator.next()!))
        }
        // Add remaining elements, if any.
        while let element = iterator.next() {
            result.append(try transform(element))
        }
        return result
    }
}
