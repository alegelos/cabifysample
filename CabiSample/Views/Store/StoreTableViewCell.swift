//
//  StoreTableViewCell.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol StoreTableViewCellProtocol: class {
    func didChange(cartItem: CartItem, amount: Int)
}

final class StoreTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var price: UILabel!
    
    private var cartItem: CartItem?
    weak var delegate: StoreTableViewCellProtocol?
    
    private var amountInt = 0 {
        didSet {
            guard let cartItem = cartItem else { return }
            delegate?.didChange(cartItem: cartItem, amount: amountInt)
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.amount.text = String(self.amountInt)
            }
        }
    }
    
    func loadCell(withCartItem cartItem: CartItem) {
        self.cartItem = cartItem
        DispatchQueue.main.async { [weak self] in
            self?.title.text  = cartItem.storeItem.name
            self?.amount.text = String(cartItem.amount)
            self?.price.text  = String(cartItem.storeItem.price) + " €"
        }
    }

    @IBAction func removeItemButtonPressed(_ sender: Any) {
        guard amountInt > 0 else { return }
        amountInt -= 1
    }
    
    @IBAction func addItemButtonPressed(_ sender: Any) {
        amountInt += 1
    }
}
