//
//  StoreTableViewHandler.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol StoreTableViewHandlerDelegate: class {
    func didSelect(item: Int)
    func reloadTable()
    func didChange(cartItem: CartItem, amount: Int)
}

extension StoreTableViewHandlerDelegate {
    func didSelect(item: Int) {}
}

final class StoreTableViewHandler: NSObject {
    weak var delegate: StoreTableViewHandlerDelegate?
    
    private enum CellType: String {
        case StoreItemCell
    }
    
    var cartItems = ContiguousArray<CartItem>() {
        didSet{
            delegate?.reloadTable()
        }
    }
}

// MARK: - UITableViewDelegate
extension StoreTableViewHandler: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(item: indexPath.row)
    }
}

// MARK: - UITableViewDataSource
extension StoreTableViewHandler: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.StoreItemCell.rawValue,
                                                       for: indexPath) as? StoreTableViewCell else {
            fatalError(AppErrors.UIError.failToDequeCell.localizedDescription)
        }
        cell.loadCell(withCartItem: cartItems[indexPath.row])
        cell.delegate = self
        return cell
    }
}

// MARK: - StoreTableViewCellProtocol
extension StoreTableViewHandler: StoreTableViewCellProtocol {
    func didChange(cartItem: CartItem, amount: Int) {
        cartItem.amount = amount
        delegate?.didChange(cartItem: cartItem, amount: amount)
    }
}
