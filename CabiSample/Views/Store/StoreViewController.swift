//
//  StoreViewController.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

final class StoreViewController: UIViewController {
    @IBOutlet weak var storeTableView: UITableView!
    
    private let storeViewModel = StoreViewModel()
    private let storeTableViewHandler = StoreTableViewHandler()
    
    enum StoreSegues: String {
        case GoToCheckOutViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupBinding()
        storeViewModel.controllerHasLoadedViewHierarchy() 
    }
    
    @IBAction func checkOutPressed(_ sender: Any) {
        storeViewModel.checkOutPressed()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let checkOutViewController as CheckOutViewController:
            let cartItemsToCheckOut = ContiguousArray(storeViewModel.checkOutItems)
            checkOutViewController.cartItemsToCheckOut(cartItemsToCheckOut)
        default:
            break
        }
    }
}

// MARK: - StoreTableViewHandlerDelegate
extension StoreViewController: StoreTableViewHandlerDelegate {
    func didChange(cartItem: CartItem, amount: Int) {
        storeViewModel.didChange(cartItem: cartItem, amount: amount)
    }
    
    func reloadTable() {
        DispatchQueue.main.async { [weak self] in
            self?.storeTableView.reloadData()
        }
    }
}

// MARK: - StoreViewModelProtocol
extension StoreViewController: StoreViewModelProtocol {
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void)) {
        DispatchQueue.main.async { [weak self] in
            self?.presentAlert(message: message, options: options, completion: completion)
        }
    }
    
    func performSegue(_ storeSegue: StoreViewController.StoreSegues, checkOutItems: ContiguousArray<CartItem>) {
        DispatchQueue.main.async { [weak self] in
            self?.performSegue(withIdentifier: storeSegue.rawValue, sender: self)
        }
    }
}

// MARK: - Private
extension StoreViewController {
    private func setupBinding() {
        storeViewModel.delegate = self
        
        storeViewModel.results.bind { [weak self] cartItems in
            self?.storeTableViewHandler.cartItems = cartItems
        }
    }
    
    private func setupTableView() {
        DispatchQueue.main.async { [weak self] in
            self?.storeTableView.tableFooterView = UIView()
        }
        storeTableView.delegate = storeTableViewHandler
        storeTableView.dataSource = storeTableViewHandler
        storeTableViewHandler.delegate = self
    }
}
