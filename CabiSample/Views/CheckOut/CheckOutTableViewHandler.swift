//
//  CheckOutTableViewHandler.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/22/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol CheckOutTableViewHandlerProtocol: class {
    func didSelect(item: Int)
    func reloadTable()
}

extension CheckOutTableViewHandlerProtocol {
    func didSelect(item: Int) {}
}

final class CheckOutTableViewHandler: NSObject {
    weak var delegate: CheckOutTableViewHandlerProtocol?
    
    private enum CellType: String {
        case CheckOutCell
    }
    
    var itemsAndPromos = [CheckOutUtilsProtocol.CartItemWithBestPromotions]() {
        didSet{
            delegate?.reloadTable()
        }
    }
}

// MARK: - UITableViewDelegate
extension CheckOutTableViewHandler: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(item: indexPath.row)
    }
}

// MARK: - UITableViewDataSource
extension CheckOutTableViewHandler: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemsAndPromos.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsCount(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellType.CheckOutCell.rawValue, for: indexPath)
        
        let data = cellData(indexPath.section, indexPath.row)
        cell.textLabel?.text = data.title
        cell.detailTextLabel?.text = data.details
        return cell
    }
}

// MARK: - Private
extension CheckOutTableViewHandler {
    typealias CellData = (title: String, details: String)
    
    private func cellsCount(_ section: Int) -> Int {
        var cellCount = 1 //If no promo to apply, show item and full price
        if let promos = itemsAndPromos[section].cartPromotions {
            cellCount = promos.count + 1
        }
        return cellCount
    }
    
    private func cellData(_ section: Int,_ row: Int) -> CellData {
        if row == 0 { // Cell for item title and total price
            let itemPrice = itemsAndPromos[section].cartItem.storeItem.price
            let itemAmount = itemsAndPromos[section].cartItem.amount
            let itemName = itemsAndPromos[section].cartItem.storeItem.name
            let itemNameAndDescription = "- " + itemName + "  " + String(itemPrice) + "€ x" + String(itemAmount)
            let totalPrice = String(itemPrice * Float(itemAmount)) + " €"
            return CellData(itemNameAndDescription, totalPrice)
        } else { // Discounts
            guard let promos = itemsAndPromos[section].cartPromotions else {
                return CellData("", "")
            }
            let promoName = "   " + promos[row-1].name
            let discountPerItem = itemsAndPromos[section].cartItem.storeItem.price - promos[row-1].price
            let totalDiscount = discountPerItem * Float(promos[row-1].amount)
            let discountString = "- " + String(totalDiscount) + " €"
            return CellData(promoName, discountString)
        }
    }
}
