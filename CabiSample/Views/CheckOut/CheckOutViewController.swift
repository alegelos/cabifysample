//
//  CheckOutViewController.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

final class CheckOutViewController: UIViewController {
    private let checkOutViewModel = CheckOutViewModel()
    private let checkOutTableViewHandler = CheckOutTableViewHandler()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        setupBinding()
    }
    
    @IBAction func didPressConfirm(_ sender: Any) {
    }
    
    func cartItemsToCheckOut(_ cartItems: ContiguousArray<CartItem>) {
        checkOutViewModel.checkOut(cartItems: cartItems)
    }
}

// MARK: - CheckOutViewModelProtocol
extension CheckOutViewController: CheckOutViewModelProtocol {
    func popViewController() {
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void)) {
        DispatchQueue.main.async { [weak self] in
            self?.presentAlert(message: message, options: options, completion: completion)
        }
    }
}

// MARK: - CheckOutTableViewHandlerProtocol
extension CheckOutViewController: CheckOutTableViewHandlerProtocol {
    func reloadTable() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Private
extension CheckOutViewController {
    private func setupTableView() {
        tableView.delegate = checkOutTableViewHandler
        tableView.dataSource = checkOutTableViewHandler
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.tableFooterView = UIView()
        }
    }
    
    private func setupBinding() {
        checkOutViewModel.itemsWithBestPromos.bind { [weak self] newValue in
            self?.checkOutTableViewHandler.itemsAndPromos = newValue
        }
        
        checkOutViewModel.totalPrice.bind { [weak self] newValue in
            self?.totalLabel.text = newValue
        }
    }
}
