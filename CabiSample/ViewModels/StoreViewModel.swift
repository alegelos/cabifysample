//
//  StoreViewModel.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
import Moya

protocol StoreViewModelProtocol: class {
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void))
    func performSegue(_ storeSegues: StoreViewController.StoreSegues, checkOutItems: ContiguousArray<CartItem>)
}

final class StoreViewModel {
    
    var results = Box(ContiguousArray<CartItem>()) // All items
    var checkOutItems = Set<CartItem>() // Items in the cart
    
    weak var delegate: StoreViewModelProtocol?
    
    
    private var state: NetworkState<ContiguousArray<StoreItem>, Error> = .loading {
        didSet {
            switch state {
            case .loading:
                break
            case .error(let error):
                print(error)
                let errorMessage = NSLocalizedString(UserErrors.NetworkErrors.requestError.rawValue,
                                                     comment: "generic network error")
                showStandarErrorAlert(errorMessage, retry: true)
            case .ready(let storeItem):
                guard !storeItem.isEmpty else { //Store items should never be empty. Did we actually run out of items?
                    let errorMessage = NSLocalizedString(UserErrors.NetworkErrors.noStoreItems.rawValue,
                                                         comment: "no item found error")
                    showStandarErrorAlert(errorMessage, retry: true)
                    return
                }
                results.value = cartItems(storeItem)
            }
        }
    }
    
    private let storeProvider: MoyaProvider<StoreServies>
    
    init(_ provider: MoyaProvider<StoreServies> = MoyaProvider<StoreServies>()) {
        storeProvider = provider
    }
    
    /// Call when controller has loaded it's view hierarchy into memory
    func controllerHasLoadedViewHierarchy() {
        storeItems()
    }
    
    /// Call to get items from Store services
    func storeItems() {
        state = .loading
        storeProvider.request(.products) { [weak self] result in
            switch result {
            case .success(let response):
                do {
                    let productsArray = try response.map(StoreItemsResponse<StoreItem>.self).products
                    let products = ContiguousArray(productsArray)
                    self?.state = NetworkState.ready(products)
                } catch {
                    self?.state = .error(error)
                }
            case .failure(let error):
                self?.state = .error(error)
            }
        }
    }
    
    func didChange(cartItem: CartItem, amount: Int) {
        if amount > 0 {
            checkOutItems.insert(cartItem)
        } else {
            checkOutItems.remove(cartItem)
        }
    }
    
    func checkOutPressed() {
        guard !checkOutItems.isEmpty else {
            let errorMessage = NSLocalizedString(UserErrors.userErrors.noItemsInCart.rawValue,
                                                 comment: "no item in cart error")
            showStandarErrorAlert(errorMessage)
            return
        }
        let contiguousArrayCheckOutItems = ContiguousArray(checkOutItems)
        delegate?.performSegue(.GoToCheckOutViewController, checkOutItems: contiguousArrayCheckOutItems)
    }
}

// MARK: - Private
extension StoreViewModel {
    private func cartItems(_ storeItems: ContiguousArray<StoreItem>) -> ContiguousArray<CartItem> {
        return storeItems.mapContiguousArray { CartItem(storeItem: $0, amount: 0) }
    }
    
    private func showStandarErrorAlert(_ message: String, retry: Bool = false) {
        let confirmationMessage = NSLocalizedString("OK", comment: "confirmation")
        delegate?.showAlert(message: message, options: confirmationMessage,
                            completion: { [weak self] _ in
                                guard retry else { return }
                                self?.storeItems() })
    }
}
