//
//  CheckOutViewModel.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
import Moya

protocol CheckOutViewModelProtocol: class {
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void))
    func popViewController()
}

final class CheckOutViewModel {
    weak var delegate: CheckOutViewModelProtocol?
    
    private let checkOutUtils: CheckOutUtilsProtocol //Util to do all checkout Logic
    private let provider: MoyaProvider<StoreServies> //Network services provider
    
    private var cartItems = ContiguousArray<CartItem>() //Items in the cart, ready to be check out
    
    // Items and promos ready to check out
    var itemsWithBestPromos = Box([CheckOutUtilsProtocol.CartItemWithBestPromotions]())
    var totalPrice = Box("")
    
    init(_ checkOutUtils: CheckOutUtilsProtocol = CheckOutUtils(),
         provider: MoyaProvider<StoreServies> = MoyaProvider<StoreServies>(stubClosure: MoyaProvider.immediatelyStub)) {
        self.checkOutUtils = checkOutUtils
        self.provider = provider
    }
    
    private var state: NetworkState<[Promotion], Error> = .loading {
        didSet {
            switch state {
            case .loading:
                break
            case .error(let error):
                print(error)
                let errorMessage = NSLocalizedString(UserErrors.NetworkErrors.requestError.rawValue,
                                                     comment: "generic network error")
                showStandarErrorAlert(errorMessage)
            case .ready(let promos):
                calculateCheckOutData(promos: promos)
                let totalPrice = checkOutUtils.totalPrice(cartItemsWithBestPromotion: itemsWithBestPromos.value)
                self.totalPrice.value = "Total price: " + String(totalPrice) + " €"
            }
        }
    }
    
    func checkOut(cartItems: ContiguousArray<CartItem>) {
        self.cartItems = cartItems
        promotions()
    }
}

// MARK: - Private
extension CheckOutViewModel {
    private func promotions() {
        provider.request(.promotions) { [weak self] result in
            switch result {
            case .success(let response):
                do {
                    let promos = try response.map(StorePromotionsResponse<Promotion>.self).promotions
                    self?.state = .ready(promos)
                } catch {
                    self?.state = .error(error)
                }
            case .failure(let error):
                self?.state = .error(error)
            }
        }
    }
    
    private func showStandarErrorAlert(_ message: String) {
        let confirmationMessage = NSLocalizedString("OK", comment: "confirmation")
        delegate?.showAlert(message: message, options: confirmationMessage,
                completion: { [weak self] _ in self?.delegate?.popViewController() })
    }
    
    private func calculateCheckOutData(promos: [Promotion]) {
        let promosByItems = self.checkOutUtils.promotionsByItems(promotions: promos)
        let itemsWithBestPromos = self.checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems,
                                                                            promotionsByItems: promosByItems)
        self.itemsWithBestPromos.value = itemsWithBestPromos
    }
}
