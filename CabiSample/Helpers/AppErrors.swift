//
//  AppErrors.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/16/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct AppErrors {
    enum UIError: Error {
        case failToDequeCell
    }
}
