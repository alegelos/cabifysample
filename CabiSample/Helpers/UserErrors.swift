//
//  UserErrors.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/19/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct UserErrors {
    enum NetworkErrors: String {
        case requestError  = "Something went wrong, pls check your internet connection and pls try again."
        case noStoreItems  = "This look weird, i can not find any item at the Store, pls try again."
    }
    
    enum userErrors: String {
        case noItemsInCart = "Please first add any item to the cart, by pressing the + button"
    }
}
