//
//  Discount.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/10/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct Promotion: Codable, Hashable {
    let code: String //Promotion code
    let name: String //Promotion name
    let discount: Float //Discount % of normal price. Value of 10 means 10% discount. Negativa values will increase the price.
    let products: Set<String> //Products to apply promotion/discount. Some values need to be added, like "all/any", "Cold beers", "TVs", "Vacations", etc.
    let amount: Int //Amount of items needed to apply discount.
    let continueAfterAmount: Bool //Yes to apply discount to next items, after fufilling the minimum amount needed.
}
