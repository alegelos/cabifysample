//
//  CartPromotion.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/11/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct CartPromotion {
    let name: String // Promotin name
    let price: Float // Price after discount
    let amount: Int  // Amount of items to apply this promotion
}
