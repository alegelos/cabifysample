//
//  StoreItem.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/10/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

final class StoreItem: Codable {
    let code: String
    let name: String
    let price: Float
    
    init(code: String, name: String, price: Float) {
        self.code = code
        self.name = name
        self.price = price
    }
}
