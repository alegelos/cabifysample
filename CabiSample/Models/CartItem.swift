//
//  CartItem.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/10/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

final class CartItem {
    let storeItem: StoreItem
    var amount: Int
    
    init(storeItem: StoreItem, amount: Int) {
        self.storeItem = storeItem
        self.amount = amount
    }
}

// MARK: - Hashable
extension CartItem: Hashable {
    var hashValue: Int {
        get {
            var hasher = Hasher()
            hash(into: &hasher)
            return hasher.finalize()
        }
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(storeItem.code)
    }
}

// MARK: - Equatable
extension CartItem: Equatable {
    static func == (lhs: CartItem, rhs: CartItem) -> Bool {
        return lhs.storeItem.code == rhs.storeItem.code
    }
}
