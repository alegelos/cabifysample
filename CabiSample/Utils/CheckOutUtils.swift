//
//  CheckOutUtils.swift
//  CabiSample
//
//  Created by Alejandro Gelos on 6/10/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

protocol CheckOutUtilsProtocol {
    typealias CartItemWithBestPromotions = (cartItem: CartItem, cartPromotions: [CartPromotion]?)
    typealias PromotionsByItems = [String: [Promotion]]
    
    func calculateCartItemWithBestPromotion(cartItems: ContiguousArray<CartItem>,
                                            promotionsByItems: PromotionsByItems) -> [CartItemWithBestPromotions]
    func totalPrice(cartItemsWithBestPromotion itemsWithPromos: [CartItemWithBestPromotions]) -> Float
    func promotionsByItems(promotions: [Promotion]) -> PromotionsByItems
    
}

struct CheckOutUtils: CheckOutUtilsProtocol {
    /// Calculate best promos and amount to apply to each item.
    ///
    /// - Parameters:
    ///   - cartItems: cartItems to apply promos
    ///   - promotionsByItems: Promotions by each item code. Prmotions must be ordered descending by the discount
    /// - Returns: All the promos and amount that must be apply to each item.
    func calculateCartItemWithBestPromotion(cartItems: ContiguousArray<CartItem>,
                            promotionsByItems: PromotionsByItems) -> [CartItemWithBestPromotions] {
        var cartItemWithBestPromotion = [CartItemWithBestPromotions]()
        for cartItem in cartItems {
            let cartItemWithBestPromos = calculateBestPromotions(toItem: cartItem, promotionsByItems: promotionsByItems)
            cartItemWithBestPromotion.append(cartItemWithBestPromos)
        }
        return cartItemWithBestPromotion
    }
    
    /// Calculate the total price of the cart with its promos
    ///
    /// - Parameter itemsWithPromos: All the promos and amount that must be apply to each item.
    /// - Returns: Final price to pay
    func totalPrice(cartItemsWithBestPromotion itemsWithPromos: [CartItemWithBestPromotions]) -> Float {
        var totalPrice: Float = 0
        for itemWithPromos in itemsWithPromos {
            var itemsLeft = itemWithPromos.cartItem.amount
            if let cartPromotions = itemWithPromos.cartPromotions {
                for cartPromo in cartPromotions {
                    itemsLeft -= cartPromo.amount
                    totalPrice += Float(cartPromo.amount) * cartPromo.price
                }
            }
            totalPrice += itemWithPromos.cartItem.storeItem.price * Float(itemsLeft)
        }
        return totalPrice
    }
    
    /// Calculate PromotionsByItems from the Promotions of the store services
    ///
    /// NOTE!!: promotions from store services MUST be ordered descending by the discount
    ///
    /// - Parameter promotions: Promotions from store services
    /// - Returns:  Promotions by each item code. Prmotions must be ordered descending by the discount
    func promotionsByItems(promotions: [Promotion]) -> PromotionsByItems {
        var promotionsByItems = PromotionsByItems()
        for promotion in promotions {
            for itemCode in promotion.products {
                if var promotionsByItem = promotionsByItems[itemCode] {
                    promotionsByItem.append(promotion)
                } else {
                    promotionsByItems[itemCode] = [promotion]
                }
            }
        }
        return promotionsByItems
    }
}


// MARK: - Private
extension CheckOutUtils {
    private func calculateBestPromotions(toItem item: CartItem,
                                promotionsByItems: PromotionsByItems) -> CartItemWithBestPromotions {
        
        guard let itemPromotions = promotionsByItems[item.storeItem.code] else {
            //No promotion for this items :-(
            return CartItemWithBestPromotions(cartItem: item, cartPromotions: nil)
        }
        var cartPromos = [CartPromotion]()
        var itemsLeft = item.amount
        for promotion in itemPromotions {
            guard itemsLeft >= promotion.amount else { continue }
            let cartPromo = bestPromo(forPrice: item.storeItem.price,
                                      amount: itemsLeft, promotion: promotion)
            itemsLeft -= cartPromo.amount
            cartPromos.append(cartPromo)
        }
        
        let promos = cartPromos.isEmpty ? nil : cartPromos
        return CartItemWithBestPromotions(cartItem: item, cartPromotions: promos)
    }
    
    private func bestPromo(forPrice price: Float, amount: Int, promotion: Promotion) -> CartPromotion {
            let priceWithPromo = price * (100 - promotion.discount) / 100
            let itemToApplyPromo = calculateItemToApplyPromo(itemsInCart: amount, promotion: promotion)
            let cartPromo = CartPromotion(name: promotion.name, price: priceWithPromo,
                                          amount: itemToApplyPromo)
            
            return cartPromo
        
    }
    
    private func calculateItemToApplyPromo(itemsInCart: Int, promotion: Promotion) -> Int {
        if promotion.continueAfterAmount {
            return itemsInCart
        }
        return (itemsInCart / promotion.amount) * promotion.amount
    }
}
