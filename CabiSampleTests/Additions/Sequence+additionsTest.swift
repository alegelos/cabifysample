//
//  Sequence+additionsTest.swift
//  CabiSampleTests
//
//  Created by Alejandro Gelos on 6/23/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import XCTest
import Moya
@testable import CabiSample

class SequenceadditionsTest: XCTestCase {
    
    var arrayOfItems: [CartItem]?
    
    override func setUp() {
        arrayOfItems = [CartItem]()
        loadArrayOfItems()
    }
    
    override func tearDown() {
        arrayOfItems = nil
    }
    
    func mapContiguousArrayTest() {
        // GIVEN
        guard let arrayOfItems = arrayOfItems else {
            XCTFail()
            return
        }
        
        // WHEN
        let result: ContiguousArray<CartItem> = arrayOfItems.mapContiguousArray {
            CartItem(storeItem: $0.storeItem, amount: 1)
        }
        
        // THEN
        let filterResults = result.filter { $0.amount == 1 }
        XCTAssertEqual(arrayOfItems.count,  result.count)
        XCTAssertEqual(filterResults.count, result.count)
    }
}

// MARK: - Private
extension SequenceadditionsTest {
    private func loadArrayOfItems() {
        let storeItem = StoreItem(code: "", name: "", price: 0)
        let cartItem = CartItem(storeItem: storeItem, amount: 0)
        for _ in 0...25 {
            arrayOfItems?.append(cartItem)
        }
    }
}
