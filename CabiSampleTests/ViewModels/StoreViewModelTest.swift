//
//  StoreViewModelTest.swift
//  CabiSampleTests
//
//  Created by Alejandro Gelos on 6/23/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import XCTest
import Moya
@testable import CabiSample

class StoreViewModelTest: XCTestCase {
    
    //Init to use sample data
    var sampleDataProvider: MoyaProvider<StoreServies>?
    var storeViewModel: StoreViewModel?
    
    var cartItems = ContiguousArray<CartItem>()
    
    var didCallToShowAlert = false
    var didCallToPerformSegue = false
    
    override func setUp() {
        sampleDataProvider = MoyaProvider<StoreServies>(stubClosure: MoyaProvider.immediatelyStub)
        storeViewModel = StoreViewModel(sampleDataProvider!)

        storeViewModel?.delegate = self

        cartItems.removeAll()
        didCallToShowAlert = false
        didCallToPerformSegue = false
        
        setupBinding()
    }
    
    override func tearDown() {
        storeViewModel = nil
        sampleDataProvider = nil
    }
    
    func testControllerHasLoadedViewHierarchy() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        
        // WHEN
        storeViewModel.controllerHasLoadedViewHierarchy()
        
        // THEN
        XCTAssertFalse(cartItems.isEmpty)
    }
    
    func testStoreItems() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        let expectedNumOfItems = 3
        
        // WHEN
        storeViewModel.controllerHasLoadedViewHierarchy()
        
        // THEN
        XCTAssertFalse(cartItems.isEmpty)
        XCTAssertEqual(cartItems.count, expectedNumOfItems)
    }
    
    func testDidChangeWithCero() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        let storeItem = StoreItem(code: "", name: "", price: 0)
        let cartItem = CartItem(storeItem: storeItem, amount: 0)
        storeViewModel.didChange(cartItem: cartItem, amount: 2)
        let expectedAmountOfItems = 0
        
        // WHEN
        storeViewModel.didChange(cartItem: cartItem, amount: 0)
        
        // THEN
        XCTAssertEqual(storeViewModel.checkOutItems.count, expectedAmountOfItems)
    }
    
    func testDidChangeWithMinus() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        let storeItem = StoreItem(code: "", name: "", price: 0)
        let cartItem = CartItem(storeItem: storeItem, amount: 0)
        storeViewModel.didChange(cartItem: cartItem, amount: 2)
        let expectedAmountOfItems = 1
        
        // WHEN
        storeViewModel.didChange(cartItem: cartItem, amount: 1)
        
        // THEN
        XCTAssertEqual(storeViewModel.checkOutItems.count, expectedAmountOfItems)
    }
    
    func testDidChangeWithPlus() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        let storeItem = StoreItem(code: "", name: "", price: 0)
        let cartItem = CartItem(storeItem: storeItem, amount: 0)
        let expectedAmountOfItems = 1
        
        // WHEN
        storeViewModel.didChange(cartItem: cartItem, amount: 1)
        
        // THEN
        XCTAssertEqual(storeViewModel.checkOutItems.count, expectedAmountOfItems)
    }
    
    func testCheckOutPressedWithItems() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        let storeItem = StoreItem(code: "", name: "", price: 0)
        let cartItem = CartItem(storeItem: storeItem, amount: 0)
        storeViewModel.didChange(cartItem: cartItem, amount: 2)
        
        // WHEN
        storeViewModel.checkOutPressed()
        
        // THEN
        XCTAssertTrue(didCallToPerformSegue)
        XCTAssertFalse(didCallToShowAlert)
    }
    
    func testCheckOutPressedWithoutItems() {
        // GIVEN
        guard let storeViewModel = storeViewModel else {
            XCTFail()
            return
        }
        
        // WHEN
        storeViewModel.checkOutPressed()
        
        // THEN
        XCTAssertTrue(didCallToShowAlert)
        XCTAssertFalse(didCallToPerformSegue)
    }
}

// MARK: - Private
extension StoreViewModelTest {
    func setupBinding() {
        storeViewModel?.results.bind { items in
            self.cartItems = items
        }
    }
}

// MARK: - StoreViewModelProtocol
extension StoreViewModelTest: StoreViewModelProtocol {
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void)) {
        didCallToShowAlert = true
    }
    
    func performSegue(_ storeSegues: StoreViewController.StoreSegues, checkOutItems: ContiguousArray<CartItem>) {
        didCallToPerformSegue = true
    }
}
