//
//  CheckOutViewModelTest.swift
//  CabiSampleTests
//
//  Created by Alejandro Gelos on 6/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import XCTest
import Moya
@testable import CabiSample

class CheckOutViewModelTest: XCTestCase {
    
    //Class to test
    var checkOutViewModel: CheckOutViewModel?
    
    //Helpers
    var checkOutUtils: CheckOutUtilsProtocol?
    var sampleDataProvider: MoyaProvider<StoreServies>?
    
    //Data
    var cartItems = ContiguousArray<CartItem>()
    var promotions = [Promotion]()
    var promotionsByItems = CheckOutUtils.PromotionsByItems()
    
    //Props for testing
    var didCallToShowAlert = false
    var didCallPopViewController = false
    var didFinishCalculatingTotalPrice = false
    var didFinishCalculatingItemsWithBestPromos = false
    
    override func setUp() {
        checkOutUtils = CheckOutUtilsMoc()
        sampleDataProvider = MoyaProvider<StoreServies>(stubClosure: MoyaProvider.immediatelyStub)
        loadCartItemsWithSampleData()
        loadPromotionsWithSampleData()
        checkOutViewModel = CheckOutViewModel(checkOutUtils!, provider: sampleDataProvider!)
        
        checkOutViewModel?.delegate = self
        
        cartItems.removeAll()
        didCallToShowAlert = false
        
        setupBinding()
    }
    
    override func tearDown() {
        checkOutViewModel = nil
        sampleDataProvider = nil
    }
    
    func testCheckOut() {
        // GIVEN
        guard let checkOutViewModel = checkOutViewModel else {
            XCTFail()
            return
        }
        
        // WHEN
        checkOutViewModel.checkOut(cartItems: cartItems)
        
        // THEN
        XCTAssertTrue(didFinishCalculatingTotalPrice)
        XCTAssertTrue(didFinishCalculatingItemsWithBestPromos)
        XCTAssertFalse(didCallToShowAlert)
    }
}

// MARK: - StoreViewModelProtocol
extension CheckOutViewModelTest: CheckOutViewModelProtocol {
    func popViewController() {
        didCallPopViewController = true
    }
    
    func showAlert(message: String, options: String..., completion: @escaping ((Int) -> Void)) {
        didCallToShowAlert = true
    }
}

// MARK: - Private
extension CheckOutViewModelTest {
    func setupBinding() {
        checkOutViewModel?.itemsWithBestPromos.bind { _ in
            self.didFinishCalculatingItemsWithBestPromos = true
        }
        
        checkOutViewModel?.totalPrice.bind { _ in
            self.didFinishCalculatingTotalPrice = true
        }
    }
    
    private func loadCartItemsWithSampleData() {
        sampleDataProvider!.request(.products) { result in
            switch result {
            case .success(let response):
                do {
                    let productsArray = try response.map(StoreItemsResponse<StoreItem>.self).products
                    let products = ContiguousArray(productsArray)
                    self.cartItems = products.mapContiguousArray { CartItem(storeItem: $0, amount: 0) }
                } catch {
                    print(error)
                    XCTFail()
                }
            case .failure(let error):
                print(error)
                XCTFail()
            }
        }
    }
    
    private func loadPromotionsWithSampleData() {
        sampleDataProvider!.request(.promotions) { result in
            switch result {
            case .success(let response):
                do {
                    let promos = try response.map(StorePromotionsResponse<Promotion>.self).promotions
                    self.promotions = promos
                } catch {
                    print(error)
                    XCTFail()
                }
            case .failure(let error):
                print(error)
                XCTFail()
            }
        }
    }
}

