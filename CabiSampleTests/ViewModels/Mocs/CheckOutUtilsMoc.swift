//
//  CheckOutUtilsMoc.swift
//  CabiSampleTests
//
//  Created by Alejandro Gelos on 6/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
@testable import CabiSample

struct CheckOutUtilsMoc: CheckOutUtilsProtocol {
    func calculateCartItemWithBestPromotion(cartItems: ContiguousArray<CartItem>, promotionsByItems: CheckOutUtilsMoc.PromotionsByItems) -> [CheckOutUtilsMoc.CartItemWithBestPromotions] {
        return []
    }
    
    func totalPrice(cartItemsWithBestPromotion itemsWithPromos: [CheckOutUtilsMoc.CartItemWithBestPromotions]) -> Float {
        return 0
    }
    
    func promotionsByItems(promotions: [Promotion]) -> CheckOutUtilsMoc.PromotionsByItems {
        return [:]
    }
}
