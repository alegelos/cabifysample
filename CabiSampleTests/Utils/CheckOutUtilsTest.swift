//
//  CheckOutUtilsTest.swift
//  CabiSampleTests
//
//  Created by Alejandro Gelos on 6/23/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import XCTest
import Moya
@testable import CabiSample

class CheckOutUtilsTest: XCTestCase {
    var checkOutUtils: CheckOutUtils?
    var sampleDataProvider: MoyaProvider<StoreServies>?
    var cartItems = ContiguousArray<CartItem>()
    var promotions = [Promotion]()
    var promotionsByItems = CheckOutUtils.PromotionsByItems()
    
    override func setUp() {
        checkOutUtils = CheckOutUtils()
        sampleDataProvider = MoyaProvider<StoreServies>(stubClosure: MoyaProvider.immediatelyStub)
        loadCartItemsWithSampleData()
        loadPromotionsWithSampleData()
        promotionsByItems = checkOutUtils!.promotionsByItems(promotions: promotions)
    }

    override func tearDown() {
        checkOutUtils = nil
        sampleDataProvider = nil
        cartItems.removeAll()
        promotions.removeAll()
        promotionsByItems.removeAll()
    }

    func testCalculateCartItemWithBestPromotionCaseOne() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
            XCTFail()
            return
        }
        cartItems.forEach { $0.amount = 1 } //Add one item of each to the cart
        
        // WHEN
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // THEN
        let voucherPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "VOUCHER" })!.cartPromotions
        let tShirtPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "TSHIRT" })!.cartPromotions
        let mugPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "MUG" })!.cartPromotions
        
        XCTAssertNil(voucherPromo)
        XCTAssertNil(tShirtPromo)
        XCTAssertNil(mugPromo)
    }
    
    func testCalculateCartItemWithBestPromotionCaseTwo() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 2
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 1
            }
        }
        let expectedVouchersWithPromo = 2
        
        // WHEN
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        
        // THEN
        let voucherPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "VOUCHER" })!.cartPromotions!
        let tShirtPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "TSHIRT" })!.cartPromotions
        let mugPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "MUG" })!.cartPromotions
        
        XCTAssertEqual(voucherPromo.count, 1)
        XCTAssertEqual(voucherPromo.first!.amount, expectedVouchersWithPromo)
        
        XCTAssertNil(tShirtPromo)
        
        XCTAssertNil(mugPromo)
    }
    
    func testCalculateCartItemWithBestPromotionCaseThree() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 1
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 4
            }
        }
        let expectedTShirtsWithPromo = 4
        
        // WHEN
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // THEN
        let voucherPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "VOUCHER" })!.cartPromotions
        let tShirtPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "TSHIRT" })!.cartPromotions!
        let mugPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "MUG" })!.cartPromotions
        
        XCTAssertEqual(tShirtPromo.count, 1)
        XCTAssertEqual(tShirtPromo.first!.amount, expectedTShirtsWithPromo)
        
        XCTAssertNil(voucherPromo)
        
        XCTAssertNil(mugPromo)
    }
    
    func testCalculateCartItemWithBestPromotionCaseFour() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 3
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 3
            }
            if $0.storeItem.code == "MUG" {
                $0.amount = 1
            }
        }
        let expectedVouchersWithPromo = 2
        let expectedTShirtsWithPromo = 3
        
        // WHEN
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // THEN
        let voucherPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "VOUCHER" })!.cartPromotions!
        let tShirtPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "TSHIRT" })!.cartPromotions!
        let mugPromo = cartItemsWithBestPromotions.first(where: { $0.cartItem.storeItem.code == "MUG"})!.cartPromotions
        
        XCTAssertEqual(voucherPromo.count, 1)
        XCTAssertEqual(voucherPromo.first!.amount, expectedVouchersWithPromo)
        
        XCTAssertEqual(tShirtPromo.count, 1)
        XCTAssertEqual(tShirtPromo.first!.amount, expectedTShirtsWithPromo)
        
        XCTAssertNil(mugPromo)
    }

    
    func testTotalPriceCaseOne() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 2
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 1
            }
        }
        let expectedPrice: Float = 25.00
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // WHEN
        let totalPrice = checkOutUtils.totalPrice(cartItemsWithBestPromotion: cartItemsWithBestPromotions)
        
        // THEN
        XCTAssertEqual(totalPrice, expectedPrice)
    }
    
    func testTotalPriceCaseTwo() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { $0.amount = 1 } //Add one item of each to the cart
        let expectedPrice: Float = 32.50
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // WHEN
        let totalPrice = checkOutUtils.totalPrice(cartItemsWithBestPromotion: cartItemsWithBestPromotions)
        
        // THEN
        XCTAssertEqual(totalPrice, expectedPrice)
    }
    
    func testTotalPriceCaseThree() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 1
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 4
            }
        }
        let expectedPrice: Float = 81.00
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // WHEN
        let totalPrice = checkOutUtils.totalPrice(cartItemsWithBestPromotion: cartItemsWithBestPromotions)
        
        // THEN
        XCTAssertEqual(totalPrice, expectedPrice)
    }
    
    func testTotalPriceCaseFour() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        cartItems.forEach { //Add one item of each to the cart
            if $0.storeItem.code == "VOUCHER" {
                $0.amount = 3
            }
            if $0.storeItem.code == "TSHIRT" {
                $0.amount = 3
            }
            if $0.storeItem.code == "MUG" {
                $0.amount = 1
            }
        }
        let expectedPrice: Float = 74.50
        let cartItemsWithBestPromotions = checkOutUtils.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
        
        // WHEN
        let totalPrice = checkOutUtils.totalPrice(cartItemsWithBestPromotion: cartItemsWithBestPromotions)
        
        // THEN
        XCTAssertEqual(totalPrice, expectedPrice)
    }
    
    func testPromotionsByItems() {
        // GIVEN
        guard let checkOutUtils = checkOutUtils,
            !promotionsByItems.isEmpty,
            !cartItems.isEmpty else {
                XCTFail()
                return
        }
        promotionsByItems.removeAll()
        let expectedTotalPromotions = 3
        let expectedPromotions = 1
        let bulkPromo = promotions.first(where: { $0.code == "bulk" })
        let voucherPromo = promotions.first(where: { $0.code == "2x1" })
        
        // WHEN
        promotionsByItems = checkOutUtils.promotionsByItems(promotions: promotions)
        
        // THEN
        XCTAssertEqual(promotionsByItems.count, expectedTotalPromotions)
        
        XCTAssertEqual(promotionsByItems["TSHIRT"]!.count, expectedPromotions)
        XCTAssertEqual(promotionsByItems["TSHIRT"]!.first!, bulkPromo)
        
        XCTAssertEqual(promotionsByItems["MUG"]!.count, expectedPromotions)
        XCTAssertEqual(promotionsByItems["MUG"]!.first!, bulkPromo)
        
        XCTAssertEqual(promotionsByItems["VOUCHER"]!.count, expectedPromotions)
        XCTAssertEqual(promotionsByItems["VOUCHER"]!.first!, voucherPromo)
    }

    func testPerformanceExample() {
        var codesToApplyPromo = Set([String]())
        for i in 0...10000 {
            // a cart with 10.000 different items
            let storeItem = StoreItem(code: String(i), name: String(i), price: Float(i))
            let newCartItem = CartItem(storeItem: storeItem, amount: 3333333)
            cartItems.append(newCartItem)

            // also with 10.000 different promotions aplicable to 10.000 different iems
            if codesToApplyPromo.count > 10000 {
                codesToApplyPromo.removeFirst()
            }
            codesToApplyPromo.insert(String(i))
            let promo = Promotion(code: String(i), name: String(i), discount: 10, products: Set(codesToApplyPromo), amount: 3, continueAfterAmount: false)
            promotions.append(promo)
        }
        
        self.measure {
            let cartItemsWithBestPromotions = checkOutUtils!.calculateCartItemWithBestPromotion(cartItems: cartItems, promotionsByItems: promotionsByItems)
            _ = checkOutUtils!.totalPrice(cartItemsWithBestPromotion: cartItemsWithBestPromotions)
        }
    }
}

// MARK: - Private
extension CheckOutUtilsTest {
    private func loadCartItemsWithSampleData() {
        sampleDataProvider!.request(.products) { result in
            switch result {
            case .success(let response):
                do {
                    let productsArray = try response.map(StoreItemsResponse<StoreItem>.self).products
                    let products = ContiguousArray(productsArray)
                    self.cartItems = products.mapContiguousArray { CartItem(storeItem: $0, amount: 0) }
                } catch {
                    print(error)
                    XCTFail()
                }
            case .failure(let error):
                print(error)
                XCTFail()
            }
        }
    }
    
    private func loadPromotionsWithSampleData() {
        sampleDataProvider!.request(.promotions) { result in
            switch result {
            case .success(let response):
                do {
                    let promos = try response.map(StorePromotionsResponse<Promotion>.self).promotions
                    self.promotions = promos
                } catch {
                    print(error)
                    XCTFail()
                }
            case .failure(let error):
                print(error)
                XCTFail()
            }
        }
    }
}
